import os
import copy
import json
import sys
import numpy as np
import pickle
import orjson
import worm_controller
import base64


def load_data(file_path, data_format, **kwargs):
    if type(file_path) is list:
        file_path = os.path.join(*file_path)

    if data_format == 'nparray':
        decoder = b64_to_np
    elif data_format == 'object':
        decoder = b64_to_obj
    else:
        raise Exception('Invalid data_format type, must be one of ["nparray", "object"].')

    data = list()

    if 'start' in kwargs and 'end' in kwargs:
        with open(file_path, 'r') as f:
            for i, line in enumerate(f):
                if kwargs['start'] <= i <= kwargs['end']:
                    data.append(decoder(line.strip('\n')))
    elif 'start' in kwargs:
        with open(file_path, 'r') as f:
            for i, line in enumerate(f):
                if kwargs['start'] <= i:
                    data.append(decoder(line.strip('\n')))
    elif 'end' in kwargs:
        with open(file_path, 'r') as f:
            for i, line in enumerate(f):
                if i <= kwargs['end']:
                    data.append(decoder(line.strip('\n')))
    else:
        with open(file_path, 'r') as f:
            for line in f:
                data.append(decoder(line.strip('\n')))

    return data


def parse_time(time_args):
    if 'sim_time' in time_args:
        time_parts = [i.strip() for i in time_args['sim_time'].split(',')]
        sim_time = 0
        for tstring in time_parts:
            time_unit = tstring[-1]
            time_amount = float(tstring[:-1])
            if time_unit == 's':
                sim_time += time_amount
            elif time_unit == 'm':
                sim_time += time_amount * 60
            elif time_unit == 'h':
                sim_time += time_amount * 60 * 60
            elif time_unit == 'd':
                sim_time += time_amount * 60 * 60 * 24
            else:
                raise Exception('Unit {} in {} not supported, must be one of ["s", "m", "h", "d"]'.format(time_unit, tstring))
        time_args['sim_time'] = int(sim_time)


    # T is the total time integrated over, from [0,T] inclusive
    # dt is the time-step of the integration
    # n is the number of points simulated, excluding the initial condition. This means n updates are evaluated
    # time_vector is the actual vector of times that we evaluate at
    if 'sim_time' in time_args and 'dt' in time_args:
        time_args['n'] = int(np.ceil(time_args['sim_time'] / time_args['dt']))
        time_args['sim_time'] = time_args['n'] * time_args['dt']
        # time_args['time_vector'] = np.linspace(0, time_args['sim_time'], time_args['n'] + 1)
        return time_args
    if 'sim_time' in time_args and 'n' in time_args:
        time_args['dt'] = time_args['sim_time'] / time_args['n']
        # time_args['time_vector'] = np.linspace(0, time_args['sim_time'], time_args['n']+1)
        return time_args
    if 'n' in time_args and 'dt' in time_args:
        time_args['sim_time'] = time_args['n'] * time_args['dt']
        # time_args['time_vector'] = np.linspace(0, time_args['sim_time'], time_args['n']+1)
        return time_args
    if 'time_vector' in time_args:
        raise Exception('Cannot handle time_vector')
        # return time_args


def obj_to_b64(obj):
    return base64.b64encode(orjson.dumps(obj.__dict__, option=orjson.OPT_SERIALIZE_NUMPY)).decode('ascii')


def b64_to_obj(base_64):
    return worm_controller.DEPController.from_dict(b64_to_dict(base_64))


def dict_to_b64(dictionary):
    return base64.b64encode(orjson.dumps(dictionary, option=orjson.OPT_SERIALIZE_NUMPY)).decode('ascii')


def b64_to_dict(base_64):
    dictionary = orjson.loads(base64.b64decode(base_64))
    recursive_list_to_np(dictionary)
    return dictionary


def np_to_b64(nparray):
    return base64.b64encode(orjson.dumps(nparray, option=orjson.OPT_SERIALIZE_NUMPY)).decode('ascii')


def b64_to_np(base_64):
    return np.array(orjson.loads(base64.b64decode(base_64)))


def obj_to_hex(obj):
    return orjson.dumps(obj.__dict__, option=orjson.OPT_SERIALIZE_NUMPY).hex()


def hex_to_obj(hexidecimal):
    return worm_controller.DEPController.from_dict(hex_to_dict(hexidecimal))


def dict_to_hex(dictionary):
    return orjson.dumps(dictionary, option=orjson.OPT_SERIALIZE_NUMPY).hex()


def hex_to_dict(hexidecimal):
    dictionary = orjson.loads(bytes.fromhex(hexidecimal))
    recursive_list_to_np(dictionary)
    return dictionary


def np_to_hex(nparray):
    return orjson.dumps(nparray, option=orjson.OPT_SERIALIZE_NUMPY).hex()


def hex_to_np(hexidecimal):
    return np.array(orjson.loads(bytes.fromhex(hexidecimal)))


def recursive_list_to_np(dictionary):
    for key, val in dictionary.items():
        if isinstance(val, list):
            dictionary[key] = np.array(val)
        elif isinstance(val, dict):
            recursive_list_to_np(val)


def serialize(data):
    return ' '.join([str(int(i)) for i in pickle.dumps(data)])


def deserialize(string):
    return pickle.loads(bytes([int(i) for i in string.split(' ')]))


def get_path(level):
    if level == 0:
        level = None
    else:
        level = -level
    return os.sep.join(os.path.abspath(__file__).split(os.sep)[0:level])


def dict_prod(key, vals, dict_list):
    dict_list_prod = []
    for val in vals:
        dict_list_copy = copy.deepcopy(dict_list)
        for dictionary in dict_list_copy:
            dictionary[key] = val
            dict_list_prod.append(dictionary)
    return dict_list_prod


def dict_factor(dictionary):
    dict_list = [copy.copy(dictionary)]
    for key in dictionary:
        vals = dictionary[key]
        dict_list = dict_prod(key, vals, dict_list)
    return dict_list


def isfloat(string):
    try:
        float(string)
        return True
    except ValueError:
        return False


def try_parse_array_spec(arg):
    if type(arg) == str:
        args = arg.split(':')
        if len(args) == 3:
            if all([isfloat(a) for a in args]):
                start = float(args[0])
                increment = float(args[1])
                end = float(args[2])
                return list(np.around(np.arange(start, end + increment, increment), 4))
            else:
                return arg
        else:
            return arg
    else:
        return arg


def parse_config_dict(dictionary):
    for key, val in dictionary.items():
        if type(val) == dict:
            parse_config_dict(val)
        else:
            dictionary[key] = try_parse_array_spec(dictionary[key])

