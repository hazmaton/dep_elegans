import os
import numpy as np
from utils import get_path


def get_capsule_volume(length, size):
    cylinder_volume = np.pi * (size ** 2) * (length * 2)
    sphere_volume = (4 / 3) * np.pi * (size ** 3)
    return cylinder_volume + sphere_volume


class Segment(object):
    body = '<body name="{name}" pos="{pos}" >'
    # geom = '<geom name="{name}" type="capsule" fromto="{fromto}" size="{size}" friction="1 0.005 0.0001"/>'
    geom = '<geom name="{name}" type="capsule" fromto="{fromto}" size="{size}" friction="0.000 0.000 0.000"/>'

    def __init__(self, index, length, size, **kwargs):
        self.index = index
        self.name = 'segment_' + str(index)
        self.length = length
        self.fromto = '0 0 0 {} 0 0'.format(str(length))
        self.size = size
        self.pos = '0 0 0'
        self.parent = None
        self.joints = list()
        self.child = None
        self.mass = get_capsule_volume(length, size)
        if 'pos' in kwargs:
            self.pos = kwargs['pos']

    def stringify(self, tab):
        string = indent(tab, Segment.body.format(name=self.name, pos=self.pos)) + '\n'
        for joint in self.joints:
            string += joint.stringify(tab+1) + '\n'
        string += indent(tab+1, Segment.geom.format(name=self.name, fromto=self.fromto, size=self.size)) + '\n'
        if self.child is not None:
            string += self.child.stringify(tab+1)
        string += indent(tab, '</body>') + '\n'
        return string


class Joint(object):
    txt = '<joint name="{name}" type="hinge" pos="0 0 0" axis="{axis}" ' \
          'range="{angle_range}" damping="5" stiffness="20" armature="0.01" />'

    def __init__(self, parent_segment, child_segment, axis, angle_range):
        self.axis = axis
        self.angle_range = angle_range
        if parent_segment is None:
            self.parent_id = None
            self.parent = None
            self.child_id = child_segment.index
            self.child = child_segment
            self.name = 'root'
        else:
            self.parent_id = parent_segment.index
            self.child_id = child_segment.index
            self.parent = parent_segment
            self.child = child_segment
            self.name = 'joint_{axis}_{parent_index},{child_index}'.format(axis=self.axis,
                                                                           parent_index=self.parent_id,
                                                                           child_index=self.child_id)

    def stringify(self, tab):
        if self.name == 'root':
            string = indent(tab, '<freejoint name="root"/>')
        else:
            string = indent(tab, Joint.txt.format(name=self.name, axis=self.axis, angle_range=self.angle_range))
        return string


class Motor(object):
    txt = '<motor name="{name}" gear="{gear}" joint="{joint}" />'

    def __init__(self, joint):
        self.name = 'actuator_{axis}_{parent_index},{child_index}'.format(axis=joint.axis,
                                                                          parent_index=joint.parent_id,
                                                                          child_index=joint.child_id)
        self.gear = int(np.round(800*((joint.child.mass * joint.parent.mass) ** (1/8))))
        # print(self.gear)
        self.joint = joint.name

    def stringify(self, tab):
        string = indent(tab, Motor.txt.format(name=self.name, gear=self.gear, joint=self.joint))
        return string


class Sensor(object):
    txt = '<jointpos name="{name}" joint="{joint}" />'

    def __init__(self, joint):
        self.name = 'sensor_{axis}_{parent_index},{child_index}'.format(axis=joint.axis,
                                                                        parent_index=joint.parent_id,
                                                                        child_index=joint.child_id)
        self.joint = joint.name

    def stringify(self, tab):
        string = indent(tab, Sensor.txt.format(name=self.name, joint=self.joint))
        return string


def indent(num_tabs, string):
    return num_tabs * '    ' + string


def join_segments(parent_segment, child_segment, axis, angle_range):
    child_segment.pos = str(parent_segment.length) + ' 0 0'
    joint = Joint(parent_segment, child_segment, axis, angle_range)
    child_segment.joints.append(joint)
    child_segment.parent = parent_segment
    parent_segment.child = child_segment
    return joint


def stringify_array(array):
    return ' '.join([str(i) for i in array])


def make_worm_strings(**kwargs):
    n_segments = kwargs['n_segments']
    if type(kwargs['segment_length']) == list:
        lengths = kwargs['segment_length']
        assert len(lengths) == n_segments
    else:
        lengths = [kwargs['segment_length'] for i in range(n_segments)]
    if type(kwargs['segment_size']) == list:
        sizes = kwargs['segment_size']
        assert len(sizes) == n_segments
    else:
        sizes = [kwargs['segment_size'] for i in range(n_segments)]
    angle_range_1 = stringify_array(kwargs['angle_range'])
    angle_range_2 = stringify_array([angle / 100 for angle in kwargs['angle_range']])
    angle_range = stringify_array(kwargs['angle_range'])
    segment_list = [Segment(i, lengths[i], sizes[i]) for i in range(n_segments)]
    segment_list[0].joints.append(Joint(None, segment_list[0], None, None))
    joint_list = list()
    for i in range(len(segment_list)-1):
        joint1 = join_segments(segment_list[i], segment_list[i+1], '0 0 1', angle_range_1)
        joint_list.append(joint1)
        joint2 = join_segments(segment_list[i], segment_list[i+1], '0 1 0', angle_range_2)
        joint_list.append(joint2)
    actuators = [Motor(joint) for joint in joint_list]
    sensors = [Sensor(joint) for joint in joint_list]

    body_string = segment_list[0].stringify(2)

    motor_string = ''
    for motor in actuators:
        motor_string += motor.stringify(2) + '\n'

    sensor_string = ''
    for sensor in sensors:
        sensor_string += sensor.stringify(2) + '\n'

    return body_string, motor_string, sensor_string


def create_worm_xml(template_file, output_file, body_string, motor_string, sensor_string, dt):
    with open(template_file, 'r') as f:
        xml_template = f.read()
    xml_string = xml_template.format(worm_body=body_string, motors=motor_string, sensors=sensor_string, timestep=dt)
    with open(output_file, 'w') as f:
        f.write(xml_string)


def make_subworm(spec):
    length = spec['length']
    start_size = spec['start_size']
    end_size = spec['end_size']
    n_segments = spec['n_segments']

    length_increment = length / n_segments
    size_increment = (end_size - start_size) / (n_segments-1)

    lengths = list()
    sizes = list()
    for i in range(n_segments):
        lengths.append(length_increment)
        sizes.append(start_size + i*size_increment)

    return lengths, sizes


def make_worm(worm_spec, xml_folder, dt):
    lengths = list()
    sizes = list()
    for sub_worm in worm_spec:
        sub_lengths, sub_sizes = make_subworm(sub_worm)
        lengths += sub_lengths
        sizes += sub_sizes
    kwargs = dict()
    kwargs['n_segments'] = len(lengths)
    kwargs['segment_length'] = lengths
    kwargs['segment_size'] = sizes
    kwargs['angle_range'] = [-50, 50]

    template_filename = os.path.join(xml_folder, 'worm_template.xml')
    worm_xml_filename = os.path.join(xml_folder, 'worm.xml')

    body_string, motor_string, sensor_string = make_worm_strings(**kwargs)
    create_worm_xml(template_filename, worm_xml_filename, body_string, motor_string, sensor_string, dt)

    return lengths, sizes

