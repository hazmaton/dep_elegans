import numpy as np


def global_normalize(kappa, array):
    rho = 1E-12
    return kappa * array / (np.linalg.norm(array, ord='fro') + rho)


def individual_normalize(kappa, array):
    rho = 1E-12
    return kappa * array / (np.sum(np.abs(array)**2, axis=-1)**(1./2) + rho)


def hebbian(x, y):
    return np.outer(y, x)


def jitter(x, k):
    # jitter_term = np.exp(-k * np.linalg.norm(x))
    # print(jitter_term)
    return x + k * np.clip(np.random.randn(*x.shape), -3, 3)


class RandomController(object):
    def __init__(self, in_dim, out_dim, **kwargs):
        self.out_dim = out_dim

    def forward(self, x_t):
        return np.random.randn(self.out_dim)


class Brain(object):
    def __init__(self, in_dim, out_dim, **kwargs):
        self.forward_model = None  # given state and action, returns prediction of next state
        self.inverse_model = None  # given next state and current state, returns action
        self.controller = None  # takes in current state and generates an action


class DEPController(object):
    def __init__(self, **kwargs):
        assert 'dt' in kwargs
        assert 'kappa' in kwargs
        assert 'in_dim' in kwargs
        assert 'out_dim' in kwargs

        for key in kwargs.keys():
            self.__setattr__(key, kwargs[key])

        if 'C' not in kwargs: self.C = np.random.randn(self.out_dim, self.in_dim)
        if 'M' not in kwargs: self.M = np.eye(self.out_dim, self.in_dim)
        if 'h' not in kwargs: self.h = np.zeros(self.out_dim)
        if 'tau_c' not in kwargs: self.tau_c = 0.1
        if 'tau_h' not in kwargs: self.tau_h = 0.1
        if 'tau_m' not in kwargs: self.tau_m = 0.1
        if 'state' not in kwargs:
            self.state = dict()
            self.state['x_t'] = np.zeros(self.in_dim)
            self.state['x_t-1'] = np.zeros(self.in_dim)
            self.state['y_t'] = np.zeros(self.out_dim)
            self.state['y_t-1'] = np.zeros(self.out_dim)

            self.state['dx_t'] = np.zeros(self.in_dim)
            self.state['dx_t-1'] = np.zeros(self.in_dim)
            self.state['dy_t'] = np.zeros(self.out_dim)
            self.state['dy_t-1'] = np.zeros(self.out_dim)

            self.state['dy_hat'] = np.zeros(self.out_dim)

    @classmethod
    def from_dict(cls, dictionary):
        K = DEPController(**dictionary)
        return K

    def shift_time(self):
        for term in ['x', 'y', 'dx', 'dy']:
            self.state['{}_t-1'.format(term)] = self.state['{}_t'.format(term)]

    def calculate_derivative(self):
        for term in ['x', 'y']:
            self.state['d{}_t'.format(term)] = (self.state['{}_t'.format(term)] - self.state['{}_t-1'.format(term)]) / self.dt

    def get_prediction(self):
        self.state['dy_hat'] = np.matmul(self.M, self.state['dx_t'])
        return self.state['dy_hat']

    def update_controller(self):
        dM = (hebbian(self.state['dx_t-1'], self.state['dy_t']) - self.M) * (self.dt / self.tau_m)
        correlation_matrix = individual_normalize(self.kappa, hebbian(self.state['dx_t'], self.get_prediction()))
        dC = (correlation_matrix - self.C) * (self.dt / self.tau_c)
        dh = -self.state['y_t'] * (self.dt / self.tau_h)
        self.M = self.M + dM
        self.C = self.C + dC
        self.h = self.h + dh

    def forward(self, x_t):
        self.shift_time()
        self.state['x_t'] = np.array(x_t)
        y_t = np.tanh(np.matmul(global_normalize(self.kappa, self.C), self.state['x_t']) + self.h)
        self.state['y_t'] = y_t
        self.calculate_derivative()
        self.update_controller()
        return self.state['y_t']

