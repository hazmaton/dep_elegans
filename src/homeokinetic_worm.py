import torch
import torch.nn as nn
import homeokinesis as hk


ex_sense_dim = 0
in_sense_dim = 10
actuator_dim = 20

state_spec = {
    'x_t': ex_sense_dim + in_sense_dim,
    's_t': ex_sense_dim,
    's_t+1': ex_sense_dim,
    '~s_t+1': ex_sense_dim,
    'u`_t': in_sense_dim,
    'u_t': in_sense_dim,
    'u_t+1': in_sense_dim,
    'epsilon_t': in_sense_dim,
    'y_t': actuator_dim
}

K2_spec = {
    'network_name': 'K_2',
    'inputs': ['s_t'],
    'outputs': ['u`_t'],
    'state_spec': state_spec,
    'layer_spec': {
        'n_layers': 3
    },
    'nonlin': nn.PReLU
}

K1_spec = {
    'network_name': 'K_1',
    'inputs': ['u`_t', 'u_t', 'epsilon_t', 's_t'],
    'outputs': ['y_t'],
    'state_spec': state_spec,
    'layer_spec': {
        'n_layers': 3
    },
    'nonlin': nn.Tanh
}

K2inv_spec = {
    'network_name': 'K^-1_2',
    'inputs': ['u_t+1'],
    'outputs': ['~s_t+1'],
    'state_spec': state_spec,
    'layer_spec': {
        'n_layers': 3
    },
    'nonlin': nn.PReLU
}
