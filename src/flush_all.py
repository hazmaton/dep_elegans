import os


main_dir = '/Users/alexbaranski/GitHub/DEP_elegans/'
sub_dirs = ['configs', 'configs_used', 'data']
for sub_dir in sub_dirs:
    directory = os.path.join(main_dir, sub_dir)
    files = os.listdir(directory)
    for file in files:
        filepath = os.path.join(directory, file)
        os.remove(filepath)
print('done')

