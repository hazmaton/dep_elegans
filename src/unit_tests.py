import numpy as np
import worm_controller
from termcolor import colored


def print_result(test_name, result, **kwargs):
    if result:
        print(colored('{} passed'.format(test_name), 'green'))
    else:
        print(colored('{} failed'.format(test_name), 'red'))


def test_hebbian():
    test_string = 'worm_controller.hebbian'
    x = np.array([1, 2, 3])
    y = np.array([4, 5, 6])
    yx = np.array([[4, 8, 12],
                   [5, 10, 15],
                   [6, 12, 18]])
    result = np.array_equal(yx, worm_controller.hebbian(x, y))
    print_result(test_string, result)


def test_DEPController():
    test_string = 'worm_controller.DEPController'
    controller = worm_controller.DEPController(in_dim=2, out_dim=3, dt=0.01, kappa=1, tau_c=0.1, tau_h=0.1)
    result = controller.C.shape == (3, 2)
    print_result(test_string + ' C shape', result)
    result = controller.M.shape == (3, 2)
    print_result(test_string + ' M shape', result)
    controller.C = np.array([[1, 2],
                             [3, 4],
                             [5, 6]])
    x_1 = np.array([1, 2])
    x_2 = np.array([2, 3])
    controller.forward(x_1)
    result = np.all(controller.state['x_t'] == np.array([1, 2]))
    print_result(test_string + ' x_t value', result)
    controller.forward(x_2)
    result = np.all(controller.state['x_t'] == np.array([2, 3])) and np.all(controller.state['x_t-1'] == np.array([1, 2]))
    print_result(test_string + ' x_t-1 value', result)
    result = np.all((controller.state['x_t']-controller.state['x_t-1']) / 0.01 == np.array([100, 100]))
    print_result(test_string + ' dx_t value', result)








test_hebbian()
test_DEPController()