import xmltodict
import os
from utils import get_path
import pickle
import worm_controller
import utils
import numpy as np
import orjson
import time
import worm_controller
import base64
import data_management
import matplotlib.pyplot as plt


def print_array(nparray):
    print('[{}]'.format(' '.join(['{:+1.3f}'.format(i) for i in nparray])))


def print_order(nparray):
    order_array = np.ceil(np.log10(np.abs(nparray))).astype(dtype=int)
    # if np.any(order_array > 4):
    print('[{}]'.format(' '.join([str(i) for i in order_array])))


def get_max_list(filepath):
    # max_list = list()
    # min_list = list()
    mean_list = list()
    with open(filepath, 'r') as f:
        for line in f:
            data = utils.b64_to_np(line.strip('\n'))
            # max_list.append(np.max(np.abs(data)))
            # min_list.append(np.min(np.abs(data)))
            mean_list.append(np.mean(np.abs(data)))
    return mean_list

folder = '/Users/alexbaranski/GitHub/DEP_elegans/data/config_0x0--[2021_02_11--18_15_43]/'

qpos_mean_list = get_max_list(folder + 'qpos')
qvel_mean_list = get_max_list(folder + 'qvel')
# qacc_mean_list = get_max_list(folder + 'qacc')
ctrl_mean_list = get_max_list(folder + 'ctrl')

# max_i = 628301

# plt.plot(qpos_min_list, 'r-.')
# plt.plot(qpos_max_list, 'r-.')
# plt.plot(qpos_mean_list, 'r-')
#
# plt.plot(qvel_min_list, 'g-.')
# plt.plot(qvel_max_list, 'g-.')
# plt.plot(qvel_mean_list, 'g-')
#
# plt.plot(qacc_min_list, 'b-.')
# plt.plot(qacc_max_list, 'b-.')
# plt.plot(qacc_mean_list, 'b-')

# plt.plot(ctrl_min_list, 'b-')
# plt.plot(ctrl_max_list, 'k-')
# plt.plot(ctrl_mean_list, 'r-')
# plt.show()

# data = utils.b64_to_np(last_line.strip('\n'))
# print_array(data)