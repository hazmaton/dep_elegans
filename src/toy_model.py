import torch
import torch.nn as nn
import numpy as np
from worm_controller import global_normalize
import matplotlib.pyplot as plt
import copy


class Physics(nn.Module):
    def __init__(self):
        super(Physics, self).__init__()
        self.linear = nn.Linear(4, 2)
        self.nonlinear = nn.Tanh()

    def forward(self, x1, x2):
        x_input = torch.cat([x1, x2], 1)
        return self.nonlinear(self.linear(x_input))


class Controller(nn.Module):
    def __init__(self, **kwargs):
        super(Controller, self).__init__()
        self.c_inv = kwargs['inverse_controller']
        self.nonlinear = nn.Tanh()

    def forward(self, x):
        temp = self.c_inv.linear.bias.data.unsqueeze(0).transpose(0, 1)
        return self.nonlinear(torch.matmul(torch.inverse(self.c_inv.linear.weight), x.transpose(0, 1) - temp).transpose(0, 1))


class InverseController(nn.Module):
    def __init__(self, **kwargs):
        super(InverseController, self).__init__()
        self.linear = nn.Linear(2, 2)
        self.optimizer = torch.optim.SGD(self.parameters(), lr=kwargs['lr'])

    def forward(self, x):
        self.linear.weight.data = global_normalize(1, self.linear.weight.data)
        return self.linear(x)


class InverseModel(nn.Module):
    def __init__(self, **kwargs):
        super(InverseModel, self).__init__()
        self.linear = nn.Linear(4, 2)
        self.optimizer = torch.optim.SGD(self.parameters(), lr=kwargs['lr'])

    def forward(self, x1, x2):
        self.linear.weight.data = global_normalize(1, self.linear.weight.data)
        x_input = torch.cat([x1, x2], 1)
        return self.linear(x_input)


K_lr = 0.5
M_lr = 0.5

criterion = nn.MSELoss()
P = Physics()
K_inv = InverseController(lr=K_lr)
K = Controller(inverse_controller=K_inv)
M_inv = InverseModel(lr=M_lr)

x_0 = torch.rand((1, 2))
x_1 = x_0 + torch.rand((1, 2))/100

l_rates = [0.5, 1]
learning_rates = dict()

models = {
        'P': P,
        'K_inv': K_inv,
        'K': K,
        'M_inv': M_inv
}

for lr in l_rates:
    learning_rates[lr] = torch.rand((0, 2))

for lr in l_rates:
    x_prev = x_0
    x_now = x_1

    learning_rates[lr] = torch.cat([learning_rates[lr], x_prev])
    learning_rates[lr] = torch.cat([learning_rates[lr], x_now])

    P = copy.deepcopy(models['P'])
    K_inv = copy.deepcopy(models['K_inv'])
    K = Controller(inverse_controller=K_inv)
    M_inv = copy.deepcopy(models['M_inv'])

    for i in range(10):
        y_pred = M_inv.forward(x_prev, x_now)
        x_pred = K_inv(y_pred)
        y = K(x_now)
        TLE = criterion(x_pred, x_prev)

        K_inv.optimizer.zero_grad()
        M_inv.optimizer.zero_grad()
        TLE.backward(retain_graph=True)
        K_inv.optimizer.step()
        M_inv.optimizer.step()

        x_prev = x_now
        x_now = P(x_prev, y)

        learning_rates[lr] = torch.cat([learning_rates[lr], x_now])

for lr in learning_rates.keys():
    x_1 = learning_rates[lr][:, 0].detach().numpy()
    x_2 = learning_rates[lr][:, 1].detach().numpy()
    plt.plot(x_1, x_2)
plt.show()