import tkinter as tk
from tkinter import filedialog
import os
from utils import get_path
from data_management import DataProcessor
import matplotlib.pyplot as plt


# root = tk.Tk()
# root.withdraw()
# file_path = filedialog.askopenfilename(initialdir=os.path.join(get_path(2), 'data'))
# root.update()
# dataprocessor = DataProcessor(file_path)
# dataprocessor.view_data()


folder = '/Users/alexbaranski/GitHub/DEP_elegans'
data_files = [i for i in os.listdir(os.path.join(folder, 'data')) if '.pickle' in i]
for data_file in data_files:
    print(data_file)
    # root = tk.Tk()
    # root.withdraw()
    # root.update()
    dataprocessor = DataProcessor(os.path.join(folder, 'data', data_file))
    dataprocessor.view_data()
    # plt.pause(0.01)

