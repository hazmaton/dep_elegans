import numpy as np
import pickle
import utils
import os
import matplotlib.pyplot as plt
import scipy.fftpack
import scipy.signal as signal


# def get_data_list(filepath):
#     data_list = list()
#     i = 0
#     with open(filepath, 'r') as f:
#         for line in f:
#             data = utils.b64_to_np(line.strip('\n'))
#             data_list.append(data)
#             i += 1
#             # if i > 1000:
#             #     break
#     return data_list
#
#
# folder = '/Users/alexbaranski/GitHub/DEP_elegans/data/config_0x0--[2021_02_12--20_48_44]/'
#
# qvel_data = get_data_list(os.path.join(folder, 'qvel'))
# ctrl_data = get_data_list(os.path.join(folder, 'ctrl'))
#
#
# with open(os.path.join(folder, 'data', 'qvel_pickled'), 'wb') as f:
#     pickle.dump(qvel_data, f)
# with open(os.path.join(folder, 'data', 'ctrl_pickled'), 'wb') as f:
#     pickle.dump(ctrl_data, f)


folder = '/Users/alexbaranski/GitHub/DEP_elegans/data/config_0x0--[2021_02_12--20_48_44]/'
# with open(os.path.join(folder, 'data', 'qvel_pickled'), 'rb') as f:
#     qvel_data = pickle.load(f)
# with open(os.path.join(folder, 'data', 'ctrl_pickled'), 'rb') as f:
#     ctrl_data = pickle.load(f)
#
# with open(os.path.join(folder, 'data', 'qvel_pickled_short'), 'wb') as f:
#     pickle.dump(qvel_data[0:10000], f)
# with open(os.path.join(folder, 'data', 'ctrl_pickled_short'), 'wb') as f:
#     pickle.dump(ctrl_data[0:10000], f)

with open(os.path.join(folder, 'data', 'qvel_pickled_short'), 'rb') as f:
    qvel_data = pickle.load(f)
with open(os.path.join(folder, 'data', 'ctrl_pickled_short'), 'rb') as f:
    ctrl_data = pickle.load(f)


n = len(qvel_data)
ctrl_array = np.zeros(n)
for i in range(n):
    ctrl_array[i] = np.mean(np.abs(ctrl_data[i]))

ctrl_hat = signal.order_filter(signal.savgol_filter(ctrl_array, 51, 0), np.ones(31), 0)
d_ctrl = ctrl_array[1:] - ctrl_array[0:-1]
# plt.plot(ctrl_array)
# plt.plot(ctrl_hat)
plt.plot(d_ctrl)
plt.show()