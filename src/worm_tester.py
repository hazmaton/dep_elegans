import os
import sys
from datetime import datetime
from worm_controller import DEPController
from worm_controller import RandomController
from worm_construction import make_worm
from utils import get_path
from data_management import DataScribe
import data_management
from data_management import DataProcessor
import numpy as np
import gc
import utils
import matplotlib.pyplot as plt
import time
from mujoco_py import load_model_from_path, MjSim, MjViewer
import traceback


# rate =
# rate = 116.74500489234924/360000
savedata = False
viz = True

dt = 0.0001
worm_spec = [
    {'length': .75, 'start_size': .02, 'end_size': .08, 'n_segments': 8},
    {'length': 1, 'start_size': .08, 'end_size': .08, 'n_segments': 7},
    {'length': .75, 'start_size': .08, 'end_size': .04, 'n_segments': 8}
]
controller_spec = {"tau_c": 5, "tau_m": 5, "tau_h": 5, "dt": dt, "kappa": 10}

lengths, sizes = make_worm(worm_spec, os.path.join(get_path(2), 'xml'), dt)
model = load_model_from_path(os.path.join(get_path(2), 'xml', 'worm.xml'))

sim = MjSim(model)
viewer = MjViewer(sim)

sim_state = sim.get_state()
# ctrl_dim = sim.data.ctrl.size
# snsr_dim = sim.data.sensordata.size
controller_spec['in_dim'] = sim.data.sensordata.size
controller_spec['out_dim'] = sim.data.ctrl.size
controller = DEPController(**controller_spec)

if savedata:
    data_filename = '[{}].txt'.format(datetime.now().strftime('%Y_%m_%d--%H_%M_%S'))
    datamanager = DataScribe(os.path.join(get_path(2), 'data'), 'config_0x0', True)
    datamanager.open_files('w')

time_args = {
    'sim_time': '10m',
    'dt': dt
}
utils.parse_time(time_args)
total_sim_steps = time_args['n']
print('Running for {} timesteps'.format(total_sim_steps))
outer_loop = True

start_time = time.time()
try:
    while outer_loop:  # outer simulation loop
        sim.set_state(sim_state)
        t = 0
        while t < total_sim_steps:  # inner simulation loop
            t += 1
            if t % 10000 == 0:
                sys.stdout.write('\r'+str(100*t/total_sim_steps)+'%')

            sim.data.ctrl[:] = controller.forward(sim.data.sensordata[:])

            if savedata:
                datamanager.write_files(sim, controller)

            # sim.data.qpos[4] = 0
            # sim.data.qvel[3] = -sim.data.qpos[4]/100
            sim.data.qacc[:] = np.clip(sim.data.qacc[:], -14000, 14000)
            # sim.data.qvel = np.clip(sim.data.qvel, -35, 35)
            sim.step()

            if viz:
                if t % 100 == 0:
                    viewer.render()
                    # time.sleep(0.5)
                # if t % 100 == 0:
                #     plt.ion()
                #     plt.imshow(controller.M)
                #     plt.show()
                #     plt.pause(0.001)

        outer_loop = False

        if os.getenv('TESTING') is not None:
            break
except Exception as e:
    track = traceback.format_exc()
    print(track)

print()
end_time = time.time()
total_run_time = end_time - start_time
print('{} seconds'.format(total_run_time))

metadata = dict()
metadata['controller'] = controller_spec
metadata['worm_body'] = worm_spec

if savedata:
    datamanager.close_and_package_files(metadata)
    # dataprocessor = DataProcessor(datamanager.directory)
    # dataprocessor.view_data()

print('\nSimulation finished.')