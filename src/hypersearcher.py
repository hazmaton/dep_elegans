import json
import os
import time
import sys
import multiprocessing
import utils
from utils import get_path
from worm_construction import make_worm
from worm_sim import WormSimulator


def flush_all(main_dir):
    sub_dirs = ['configs', 'configs_used', 'data']
    return_val = []
    for sub_dir in sub_dirs:
        directory = os.path.join(main_dir, sub_dir)
        files = os.listdir(directory)
        if sub_dir == 'configs':
            return_val = files
        for file in files:
            filepath = os.path.join(directory, file)
            os.remove(filepath)
    return return_val


def create_config_files(folder):
    # creating the initial config files
    with open(os.path.join(folder, 'json', 'config.json'), 'r') as f:
        config = json.load(f)
    utils.parse_config_dict(config)
    all_configs = utils.dict_factor(config)
    configs_dict = dict()
    for i, c in enumerate(all_configs):
        configs_dict['config_{}.json'.format(hex(i))] = c
    for filename in configs_dict.keys():
        with open(os.path.join(folder, 'configs', filename), 'w') as f:
            json.dump(configs_dict[filename], f)

# we will have a main process which keeps track of the config files that are 'claimable'
def process(filename):
    folder = '/Users/alexbaranski/GitHub/DEP_elegans/'
    with open(os.path.join(folder, 'configs', filename), 'r') as f:
        config = json.load(f)
        worm_sim = WormSimulator(controller_spec=config, config_name=filename, time_steps=10000)
        worm_sim.simulate()

    with open(os.path.join(folder, 'configs_used', filename), 'w') as f:
        json.dump(config, f)
    os.remove(os.path.join(folder, 'configs', filename))
    # print(os.getpid())


main_dir = '/Users/alexbaranski/GitHub/DEP_elegans/'
worm_spec = [
    {'length': .75, 'start_size': .02, 'end_size': .08, 'n_segments': 8},
    {'length': 1, 'start_size': .08, 'end_size': .08, 'n_segments': 7},
    {'length': .75, 'start_size': .08, 'end_size': .02, 'n_segments': 8}
]
parallel = False

if __name__ == '__main__':
    flush_all(main_dir)
    create_config_files(main_dir)
    jobs = os.listdir(os.path.join(main_dir, 'configs'))
    # num_workers = multiprocessing.cpu_count() - 3

    lengths, sizes = make_worm(worm_spec, os.path.join(get_path(2), 'xml'))

    num_workers = 20
    # if num_workers < 1:
    #     num_workers = 1

    # t_start = time.time()
    if parallel:
        with multiprocessing.Pool(num_workers) as p:
            p.map(process, jobs,)
            p.close()
            p.join()
    else:
        for job in jobs:
            process(job)
    # t_end = time.time()
    # t_total = t_end - t_start

