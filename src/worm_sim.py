import os
import sys
from worm_controller import DEPController
from worm_controller import RandomController
from worm_construction import make_worm
from utils import get_path
from data_management import DataScribe
import numpy as np
from mujoco_py import load_model_from_path, MjSim
import xmltodict


class WormSimulator(object):
    def __init__(self, **kwargs):
        assert 'controller_spec' in kwargs
        assert 'config_name' in kwargs
        assert 'time_steps' in kwargs
        self.controller_spec = kwargs['controller_spec']
        self.config_name = kwargs['config_name'].replace('config_', '').replace('.json', '')
        self.time_steps = kwargs['time_steps']
        if 'worm_spec' in kwargs:
            self.worm_spec = kwargs['worm_spec']
        else:
            self.worm_spec = [
                {'length': .75, 'start_size': .02, 'end_size': .08, 'n_segments': 8},
                {'length': 1, 'start_size': .08, 'end_size': .08, 'n_segments': 7},
                {'length': .75, 'start_size': .08, 'end_size': .02, 'n_segments': 8}
            ]

        # self.lengths, self.sizes = make_worm(self.worm_spec, os.path.join(get_path(2), 'xml'))
        self.model = load_model_from_path(os.path.join(get_path(2), 'xml', 'worm.xml'))
        with open(os.path.join(get_path(2), 'xml', 'worm.xml')) as f:
            model_xml = xmltodict.parse(f.read())
            self.controller_spec['dt'] = float(model_xml['mujoco']['option']['@timestep'])

    def simulate(self):
        sim = MjSim(self.model)
        # sim_state = sim.get_state()
        ctrl_dim = sim.data.ctrl.size
        snsr_dim = sim.data.sensordata.size
        controller = DEPController(snsr_dim, ctrl_dim, **self.controller_spec)

        datamanager = DataScribe(os.path.join(get_path(2), 'data'), self.config_name)
        datamanager.open_files('w')
        # sim.set_state(sim_state)
        for t in range(self.time_steps):
            datamanager.write_files(sim, controller)
            sim.data.ctrl[:] = controller.forward(sim.data.sensordata[:])
            sim.step()

        metadata = dict()
        metadata['controller'] = self.controller_spec
        metadata['worm_body'] = self.worm_spec

        datamanager.close_and_package_files(metadata)

