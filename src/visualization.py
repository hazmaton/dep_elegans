import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import utils as utils


start = 0
end = 200000

datadir = '/Users/alexbaranski/GitHub/DEP_elegans/data/config_0x0--[2021_02_12--20_48_44]'
print('Loading ctrl data...')
ctrl_data = utils.load_data([datadir, 'ctrl'], 'nparray')
print('Calculating mean values...')
mean_ctrl_data = np.array([np.mean(np.abs(i)) for i in ctrl_data])

np_ctrl_data = np.concatenate([np.expand_dims(i, 1) for i in ctrl_data], 1)
f0 = plt.figure(figsize=(15, 5))
for i in range(np_ctrl_data.shape[0]):
    plt.plot(np_ctrl_data[i, :])

d_mean_ctrl_data = mean_ctrl_data[1:]-mean_ctrl_data[:-1]
f1 = plt.figure(figsize=(15, 5))
plt.plot(d_mean_ctrl_data[0:10000])

# 2500 to 2600 there is a big 'collapse'
# print('Loading brain data...')
# brain_data = utils.load_data([datadir, 'brain'], 'object', start=start, end=end)
# print('done loading data')

m_max = 1
c_max = 1

f2, axs = plt.subplots(1, 2)
plt.subplots_adjust(left=0.1, bottom=0.25)
im = np.random.rand(12, 12)
im1 = axs[0].imshow(im, vmin=-0.1, vmax=0.1)
im2 = axs[1].imshow(im, vmin=-30, vmax=30)
axs[0].margins(x=0)
axs[1].margins(x=0)

axcolor = 'lightgoldenrodyellow'
axfreq = plt.axes([0.1, 0.1, 0.75, 0.03], facecolor=axcolor)

sfreq = Slider(axfreq, 'Freq', start, end, valinit=0, valstep=1)


def update(val):
    M = brain_data[val-start].M
    C = brain_data[val-start].C

    im1.set_data(M)
    im2.set_data(C)
    f2.canvas.draw_idle()


sfreq.on_changed(update)


plt.show()